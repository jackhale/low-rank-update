# This program recreates some ideas from Example 1 of the paper "Optimal
# low-rank approximations of Bayesian linear inverse problems" Spantini et al.
# (in press, v2) available at http://arxiv.org/abs/1407.3463.
# Author Jack S. Hale, University of Luxembourg, 2015 <jack.hale@uni.lu>
# License: LGPL

import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg

# NOTE: Set seed here for debugging.
#np.random.seed(1)

first_plot = True

def main():
    # number of parameters
    size = 100
    k = np.arange(1, size + 1, dtype=np.float)

    # Prior spectrum parameters
    lmbda_0_p = 1.0
    alpha_p = 2.0
    tau_p = 10.0E-6

    # Hessian spectrum parameters
    lmbda_0_H = 500.0
    alpha_H = 0.345 
    tau_H = 10.0E-6
    
    # Prior spectrum
    Lmbda_p = (lmbda_0_p/k**alpha_p) + tau_p
    # Hessian spectrum
    Lmbda_H = (lmbda_0_H/k**alpha_H) + tau_H

    # Setup plots
    plt.subplot(211)
    plt.xlim(np.min(k), np.max(k))
    plt.xticks(np.arange(min(k), max(k) + 1, 9.0))
    plt.xlabel(r'index $k$')
    plt.ylabel(r'eigenvalue $\lambda_k$')
    
    # Plot eigenspectrum
    plt.subplot(211)
    ax1 = plt.semilogy(k, Lmbda_p, 'b', label=r'$\Gamma_{\mathrm{prior}}$')
    ax2 = plt.semilogy(k, 1.0/Lmbda_H, 'r', label=r'$H^{-1}$')

    plt.subplot(212)
    plt.xlim(np.min(k), np.max(k))
    plt.xticks(np.arange(min(k), max(k) + 1, 9.0))
    plt.xlabel(r'rank of update')
    plt.ylabel(r'metric $d_{\mathcal{F}}$')

    # Perform the low-rank update
    realisations = 50
    for _ in range(realisations):
        compute_low_rank_updates(Lmbda_p, Lmbda_H, method="pencil")
    
    plt.subplot(211)
    plt.legend()
    plt.subplot(212)
    plt.legend()
    plt.show()

def low_rank(U, Lmbda, rank):
    """Given eigenvectors in the columns of U and associated eigenvalues Lmbda
    sort them in descending order and return the low-rank truncated version."""
    # NOTE: Always sort your eigenvalues! No guarantees from LAPACK.
    # http://mail.scipy.org/pipermail/numpy-discussion/2007-February/026126.html
    ind = np.argsort(Lmbda)[::-1]
    Lmbda = Lmbda[ind]
    U = U[:, ind]
    # Truncate
    Lmbda_lr = Lmbda[0:(rank + 1)]
    U_lr = np.asmatrix(U[:, 0:(rank + 1)])
    return (U_lr, Lmbda_lr)


def generate_orthogonal_matrix(size):
    """Return an orthogonal matrix computed by performing a QR decomposition on
    a matrix with random entries."""
    M = np.random.normal(size=(size, size))
    Q, R = scipy.linalg.qr(M, mode='full')
    return np.asmatrix(Q)


def forstner_metric(A, B):
    """Given two SPD matrices A and B calculate the Forstner metric between
    them by solving the generalised eigenvalue problem on the pencil (A, B).

    See: "A Metric for Covariance Matrices" Forstner. 
    http://www.ipb.uni-bonn.de/fileadmin/publication/pdf/Forstner1999Metric.pdf"""
    sigma = scipy.linalg.eigh(A, B, eigvals_only=True)
    d_f = np.sqrt(np.sum(np.log(sigma)**2))
    return d_f


def compute_low_rank_updates(Lmbda_p, Lmbda_H, method="pencil"):
    assert (len(Lmbda_p) == len(Lmbda_H))
    size = len(Lmbda_p)
    k = np.arange(1, size + 1, dtype=np.float)
    
    # Generate random orthogonal matrices
    # NOTE: In the paper it says we cannot use Householder reflections, and Gram-Schmidt
    # must be used. However, I've had no problems using QR routines. Investigate further.
    # Prior
    V_p = generate_orthogonal_matrix(size)
    # Hessian
    V_H = generate_orthogonal_matrix(size)

    # Hessian
    H = V_H*np.asmatrix(np.diag(Lmbda_H))*V_H.T
    # Prior covariance
    Gamma_prior = np.asmatrix(V_p*np.asmatrix(np.diag(Lmbda_p))*V_p.T)
    # Inverse of the prior covariance
    Gamma_prior_inv = np.asmatrix(V_p*np.asmatrix(np.diag(1.0/Lmbda_p))*V_p.T)
    # Exact posterior covariance
    Gamma_pos = np.linalg.inv(H + Gamma_prior_inv)
    
    if method == "pencil":
        # Matrix pencil problem of (H, Gamma_prior_inv)
        Delta, W_hat = scipy.linalg.eigh(H, b=Gamma_prior_inv)
    elif method == "squareroot":
        # In the case that a square root factorisation of the prior covariance
        # is available (here done with Cholesky) then we can alternatively
        # solve a standard eigenvalue problem
        S = np.asmatrix(scipy.linalg.cholesky(Gamma_prior, lower=True))
        # See section 2.4 Spantini et al.
        Delta, W = scipy.linalg.eigh(S.T*H*S) 
    else:
        raise ValueError("Valid options for value method are pencil and squareroot.")
    
    forstner_metrics_H = np.zeros(size - 1)
    forstner_metrics_O = np.zeros(size - 1)
    forstner_metrics_p = np.zeros(size - 1)
    for rank in np.arange(0, size - 1):
        # Calculate Hessian-based low-rank update
        V_H_lr, Lmbda_H_lr = low_rank(V_H, Lmbda_H, rank=rank)
        Gamma_pos_lr_H = Gamma_prior - \
            Gamma_prior*V_H_lr*(np.linalg.inv(np.diag(1.0/Lmbda_H_lr) \
            + V_H_lr.T*Gamma_prior*V_H_lr))*V_H_lr.T*Gamma_prior
        
        # Calculate prior-based low-rank update
        V_p_lr, Lmbda_p_lr = low_rank(V_p, Lmbda_p, rank=rank)
        T = np.asmatrix(np.diag(Lmbda_p_lr))
        U = V_p_lr
        Gamma_pos_lr_p = Gamma_prior - U*T*np.linalg.inv(np.linalg.inv(U.T*H*U) + T)*T*U.T

        # Calculate generalized low-rank update
        if method == "pencil":
            W_hat_lr, Delta_lr = low_rank(W_hat, Delta, rank=rank)
            Gamma_pos_lr_O = Gamma_prior - W_hat_lr*np.asmatrix(np.diag(Delta_lr/(1.0 + Delta_lr)))*W_hat_lr.T
        elif method == "squareroot":
            W_lr, Delta_lr = low_rank(W, Delta, rank=rank)
            Gamma_pos_lr_O = S*(np.identity(size) - W_lr*np.asmatrix(np.diag(Delta_lr/(1.0 + Delta_lr)))*W_lr.T)*S.T

        # Calculate distances in Forstner metric between the low-rank and exact
        # posterior
        forstner_metrics_H[rank] = forstner_metric(Gamma_pos_lr_H, Gamma_pos)
        forstner_metrics_O[rank] = forstner_metric(Gamma_pos_lr_O, Gamma_pos)
        forstner_metrics_p[rank] = forstner_metric(Gamma_pos_lr_p, Gamma_pos)

    plt.subplot(212)
    
    global first_plot
    ax1, = plt.semilogy(k[0:-1], forstner_metrics_H, 'g') 
    if first_plot: ax1.set_label(r'Hessian')
    
    ax2, = plt.semilogy(k[0:-1], forstner_metrics_p, 'b')
    if first_plot: ax2.set_label(r'Prior')

    ax3, = plt.semilogy(k[0:-1], forstner_metrics_O, 'r')
    if first_plot: ax3.set_label(r'Generalized')
    

    first_plot = False

if __name__ == "__main__":
    main()
