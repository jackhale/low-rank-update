# Numpy: Optimal low-rank approximations for Bayesian linear inverse problems ##
## Code Author: Jack S. Hale, Paper Authors: Spantini, Solonen, Cui, Martin, Tenorio, Marzouk.

This Python script (numpy, scipy, matplotlib required) implements Example 1
from the paper "Optimal low-rank approximations of Bayesian linear inverse
problems" by Spantini et al. 

<http://arxiv.org/abs/1407.3463>

Theorem 2.3 in this paper states that when updating the prior covariance to the
posterior covariance, the optimal low-rank update is the one that minimises the
Förstner metric between the low-rank update and the exact posterior covariance.
Performing this update involves solving a generalised eigenvalue problem on the
matrix pencil defined by the Hessian and the inverse of the prior covariance.
This script shows how to do that using numpy and scipy and produces a plot
similar to the one in the paper.

![Plot](fig5point1leftcolumn.png)